# -*- coding: utf-8 -*-
"""
Created on Wed Nov  7 10:34:28 2018

@author: Mark Krumwiede
Final Project
Raster - NDVIs
"""

import arcpy, os
import numpy as np
import matplotlib.pyplot as plt
import math

inMaskData = arcpy.GetParameterAsText(0)
rasters = arcpy.GetParameterAsText(1).split(';')
output_plot = arcpy.GetParameterAsText(2)
outFolder = arcpy.GetParameterAsText(3)

#check out spatial analyst tool
arcpy.CheckOutExtension('spatial')

s = 1

plt.figure(figsize=(8,11)) 
plt.subplots(1,1,figsize=(18,18))

#This will loop through each composite raster in the folder and clip to a shapefile, then calculate statistics on them, and NDVI
#loop through list of rasters
for inRaster in rasters:
    #set the output name for each output to be the same as the input
    outRasterarray = inRaster.split('\\')
    outRasterlength = len(outRasterarray) - 1
    outRasterstring = outRasterarray[outRasterlength]
    outRasterOutput = outFolder + '\\' + outRasterstring
    #outRaster = inRaster.replace(".img", "clip.img") TESTING THIS
    outRaster = outRasterOutput.replace("img","clip.img")
    #we want an subplot with 4 columns
    r = len(rasters)/4
    if len(rasters)%4 > 0:
        r = r + 1

     #process: Extract by Mask  
    arcpy.gp.ExtractByMask_sa(inRaster, inMaskData, outRaster)
    #calculate statistics on each raster
    arcpy.CalculateStatistics_management(outRaster)
    
    #calculate math on each raster
    #get the list of bands from each raster
    bands = arcpy.Describe(outRaster).children 
    #create a Raster object from the red (3) band.
    
    if arcpy.Describe(inRaster).bandCount==7:
        red = arcpy.Raster(bands[3].catalogPath)
        #create a Raster object from the NIR (4) band.
        nir = arcpy.Raster(bands[4].catalogPath)
    else:
        red = arcpy.Raster(bands[2].catalogPath)
        #create a Raster object from the NIR (3) band.
        nir = arcpy.Raster(bands[3].catalogPath)
        #calculate the NDVI, making sure to turn the numerator to floating point so it does floating point math.
    ndvi = arcpy.sa.Float(nir - red) / (nir + red)
    #save the NDVI rasters
    ndvi.save(outRaster.replace(".img", "NDVI.img"))
    

    n = os.path.basename(inRaster)
    name = n[15:19]
    #send to numpy array to begin displaying in subplots
    plot = arcpy.RasterToNumPyArray(ndvi, nodata_to_value=-2)
    ndvi_data_masked = np.ma.masked_equal(plot, -2)
    stats = "Mean: {0:.2}\nSD: {1:.2}".format(ndvi.mean, ndvi.standardDeviation)
    plt.subplot(r, 4, s)
    plt.subplots_adjust(wspace = 0.6)
    plt.text(0.1, -0.3, stats, transform=plt.gca().transAxes)
    plt.title(name + " NDVI")
    plt.axis('off')
    mean, sd = ndvi.mean, ndvi.standardDeviation * 2
    plt.imshow(ndvi_data_masked, norm=plt.Normalize(mean - sd, mean + sd), cmap = 'gist_yarg')
    s = s + 1

#how to save in separate folder?
plt.savefig(output_plot, transparent=True)

arcpy.AddMessage('About Damn Time!')