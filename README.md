# Clip Rasters to Shapefile and Generate Multiple NDVIs

This tool takes multiple inputs to give you a subplot output of multiple NDVIs for use on your projects. It will take a shapefile boundary of your choosing, I am working on post-
fire revegetation analysis for for a fire in Idaho, so I used the boundary from that fire. In order for this tool to work you must have already made composite bands for each of your
raster datasets. Once the bands have been created you can input them in to the tool. Next you will create a name for the subplot .pdf file to be created, and lastlty  you will 
choose the folder that will house the output from the tool which includes the suplot .pdf file with the clipped rasters in NDVIs. As well as eacher raster clipped to the boundary, 
and a NDVI generated for each raster as well.

Boundary to Clip:
Whatever shapefile boundary you want your data clipped to

Composite Rasters:
Any amount of rasters that you want to work with for you project. Since the output spits the datasets into a suplot with the years being the titles, it would be best to add the 
rasters in a yearly order, since that is how the subplot will display. Ex. you have a bunch a yearly raster starting 1990 to 2018. If you add 2018 first, then 1990-2017, the plot
will display the 2018 image first, then the 1990-2017 ones.

File Type:
This is where you name the subplot that will be generated, it automatically calls for it to be put in .pdf format.

Output Folder:
This is where your clipped rasters, NDVIs, and subplot .pdf will be stored when the tool has finished running. It would be best practice to create a folder called "Output", or 
some other similar term to help when trying to find the data when it is finished running.
